﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class MouseManager : MonoBehaviour
{
    // Para saber en que objetos puedes hacer click

    public LayerMask clickableLayer;

    public int publicIntTest;

    //cambia cursor segun objeto

    public Texture2D pointer; //cursor normal
    public Texture2D target; //para los objetos clickeables del mundo
    public Texture2D doorway; //puerta
    public Texture2D combat; //para combate

    public EventVector3 OnClickEnviroment;


    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 50, clickableLayer.value))
        {
            bool door = false;
            bool item = false;


            if (hit.collider.gameObject.tag == "Doorway")
            {
                Cursor.SetCursor(doorway, new Vector2(16, 16), CursorMode.Auto);
                door = true;

            }

            else if (hit.collider.gameObject.tag == "Item")
            {
                Cursor.SetCursor(combat, new Vector2(16, 16), CursorMode.Auto);
                item = true;
            }
            else
            {
                Cursor.SetCursor(target, new Vector2(16, 16), CursorMode.Auto);
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (door)
                {
                    Transform Doorway = hit.collider.gameObject.transform;

                    OnClickEnviroment.Invoke(Doorway.position);
                    Debug.Log("DOOR");
                }else if (item)
                {
                    Transform itemPos = hit.collider.gameObject.transform;
                    OnClickEnviroment.Invoke(itemPos.position);
                    Debug.Log("ITEM");
                }
                else
                {
                    OnClickEnviroment.Invoke(hit.point);
                }


            }
        }

        else
        {
            Cursor.SetCursor(pointer, Vector2.zero, CursorMode.Auto);
        }

    }
}
[System.Serializable]

public class EventVector3 : UnityEvent<Vector3> { }
